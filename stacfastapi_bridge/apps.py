# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0
from django.apps import AppConfig


class StacfastapiBridgeConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "stacfastapi_bridge"
