# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0
from django.urls import path

from . import api

urlpatterns = [
    path("stacapi/", api.app_editor, name="app_editor"),
    path("stac_api_ui/", api.stac_api_ui, name="stac_api_ui"),
]
