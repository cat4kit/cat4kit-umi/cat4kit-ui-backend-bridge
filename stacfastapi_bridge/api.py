# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0
import os

from django.http import JsonResponse
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def app_editor(request):
    extensions_list = [i["code"] for i in request.data["extensions"]]

    with open("/app/cat4kit/app.py", "r") as input_file, open(
        "/app/cat4kit/app2.py", "w"
    ) as output_file:
        for line in input_file:
            if "title_arbitrary = " in line:
                before_keyword, keyword, after_keyword = line.partition(
                    "title_arbitrary = "
                )
                output_file.write(
                    line.replace(
                        before_keyword + keyword + after_keyword,
                        before_keyword
                        + keyword
                        + "'"
                        + request.data["title"].strip()
                        + "'\n",
                    )
                )
            elif "description_arbitrary = " in line:
                before_keyword, keyword, after_keyword = line.partition(
                    "description_arbitrary = "
                )
                output_file.write(
                    line.replace(
                        before_keyword + keyword + after_keyword,
                        before_keyword
                        + keyword
                        + "'"
                        + request.data["description"]
                        .strip()
                        .replace("\n", "\\n\\n")
                        .replace('"', "")
                        + "'\n",
                    )
                )
            elif "landing_page_id_arbitrary = " in line:
                before_keyword, keyword, after_keyword = line.partition(
                    "landing_page_id_arbitrary = "
                )
                output_file.write(
                    line.replace(
                        before_keyword + keyword + after_keyword,
                        before_keyword
                        + keyword
                        + "'"
                        + request.data["landing_page_id"].strip()
                        + "'\n",
                    )
                )
            elif 'os.environ["ENABLED_EXTENSIONS"] = ' in line:
                before_keyword, keyword, after_keyword = line.partition(
                    'os.environ["ENABLED_EXTENSIONS"] = '
                )
                print(",".join(extensions_list))
                output_file.write(
                    line.replace(
                        before_keyword + keyword + after_keyword,
                        before_keyword
                        + keyword
                        + "'"
                        + ",".join(extensions_list).strip()
                        + "'\n",
                    )
                )
            else:
                output_file.write(line)

    os.remove("/app/cat4kit/app.py")

    os.rename("/app/cat4kit/app2.py", "/app/cat4kit/app.py")
    return JsonResponse({"message": "successfull"})


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def stac_api_ui(request):
    with open("/app/cat4kit/app.py", "r") as input_file:
        for line in input_file:
            if "title_arbitrary = " in line:
                before_keyword, keyword, title_arbitrary = line.partition(
                    "title_arbitrary = "
                )
            elif "description_arbitrary = " in line:
                (
                    before_keyword,
                    keyword,
                    description_arbitrary,
                ) = line.partition("description_arbitrary = ")
            elif "landing_page_id_arbitrary = " in line:
                (
                    before_keyword,
                    keyword,
                    landing_page_id_arbitrary,
                ) = line.partition("landing_page_id_arbitrary = ")
            elif 'os.environ["ENABLED_EXTENSIONS"] = ' in line:
                (
                    before_keyword,
                    keyword,
                    ENABLED_EXTENSIONS_arb,
                ) = line.partition('os.environ["ENABLED_EXTENSIONS"] = ')
                extensions_list = ENABLED_EXTENSIONS_arb.strip().split(",")
                extensions_list = [
                    {
                        "name": ele.title().replace("'", ""),
                        "code": ele.replace("'", ""),
                    }
                    for ele in extensions_list
                ]

    return JsonResponse(
        {
            "title": title_arbitrary.strip(),
            "description": description_arbitrary.strip(),
            "landing_page_id": landing_page_id_arbitrary.strip(),
            "extensions": extensions_list,
        }
    )
